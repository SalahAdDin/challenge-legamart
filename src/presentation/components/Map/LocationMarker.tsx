/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable no-underscore-dangle */
import { LocationT } from '@domain/location.dto';
import L from 'leaflet';
import React, { memo, useRef } from 'react';
import { Marker, Popup } from 'react-leaflet';
import Button from '../button/Button';

import styles from './LocationMarker.module.css';

const LocationMarker: React.FC<Omit<LocationT, 'id'>> = ({
  name,
  type,
  location,
  logo,
}) => {
  const popupRef = useRef<L.Popup>(null);

  const closePopUp = () => {
    if (!popupRef.current) return;
    //  @ts-ignore
    popupRef.current._closeButton.click();
  };

  return (
    <Marker position={location}>
      <Popup className={styles.marker_popup} ref={popupRef} closeButton={false}>
        <header>
          <h3>{name}</h3>
        </header>
        <main>
          <h6>{type}</h6>
          <img alt={`${name}'s logo.`} title={`${name}'s logo.`} src={logo} />
        </main>
        <footer>
          <Button type="submit" disabled>
            Submit
          </Button>
          <Button secondary onClick={closePopUp}>
            Cancel
          </Button>
        </footer>
      </Popup>
    </Marker>
  );
};

export default memo(LocationMarker);
