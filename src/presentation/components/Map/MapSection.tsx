import 'leaflet/dist/leaflet.css';

import AppContext from '@application/context';
import { GeometryT } from '@domain/location.dto';
import React, { useEffect, useState } from 'react';
import { MapContainer, TileLayer } from 'react-leaflet';
import LocationMarker from './LocationMarker';

const MapSection = () => {
  const { locations } = React.useContext(AppContext);

  const [center, setCenter] = useState<GeometryT>();

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      });
    }
  }, []);

  return center !== undefined ? (
    <MapContainer
      center={center}
      scrollWheelZoom
      zoom={10}
      style={{ height: '100%' }}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {locations.length > 0 &&
        locations.map((location) => (
          <LocationMarker key={location.id} {...location} />
        ))}
    </MapContainer>
  ) : (
    <div />
  );
};

export default MapSection;
