import React, { memo } from 'react';

type WeatherIconProps = {
  icon: React.FC;
  size?: number | undefined;
  label?: string | undefined;
  style?: React.CSSProperties | undefined;
};

const Icon: React.FC<WeatherIconProps> = ({
  icon,
  size,
  label,
  style,
  ...restProps
}) => {
  const Component = icon;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={`${size || 24}px`}
      height={`${size || 24}px`}
      viewBox="0 0 24 24"
      role={label ? 'img' : 'presentation'}
      aria-label={label || undefined}
      style={style}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    >
      <Component />
    </svg>
  );
};

export default memo(Icon);
