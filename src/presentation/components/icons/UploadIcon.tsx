import React from 'react';

const UploadIcon: React.FC = () => (
  <path d="M2 12h2v5h16v-5h2v5c0 1.11-.89 2-2 2H4a2 2 0 01-2-2v-5M12 2L6.46 7.46l1.42 1.42L11 5.75V15h2V5.75l3.13 3.13 1.42-1.43L12 2z" />
);

export default UploadIcon;
