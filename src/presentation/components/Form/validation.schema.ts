import { mixed, object, string } from 'yup';

const schema = object({
  name: string()
    .required('Father, give me a name!')
    .min(5, 'Minimum 5 characters.')
    .max(15, 'Maximum 15 characters.'),
  location: object({}).required('Father, where is the location?'),
  type: string().required('Father, choose a type!'),
  logo: mixed()
    .test('required', "Father, where is the logo's url?", (file: File) => {
      if (file) return true;
      return false;
    })
    .test(
      'fileSize',
      'File Size is too large!',
      (file: File) => file && file.size <= 2000000,
    )
    .test(
      'fileType',
      'Unsupported File Format!',
      (file: File) =>
        file && ['image/jpeg', 'image/png', 'image/jpg'].includes(file.type),
    ),
});

export default schema;
