import { Controller, FieldValues, UseControllerProps } from 'react-hook-form';

import styles from '../form.module.css';

type OptionsT = { label: string; value: string };

const SelectInput = <T extends FieldValues>({
  name,
  control,
  label,
  options,
}: UseControllerProps<T> & { label: string; options: OptionsT[] }) => (
  <Controller
    name={name}
    control={control}
    render={({ field, fieldState: { error } }) => (
      <div className={styles.field}>
        <label htmlFor={name}>{`${label}:`}</label>
        <select {...field}>
          {options.map((option) => (
            <option
              value={option.value}
              key={`select-input-option-${option.value}`}
            >
              {option.label}
            </option>
          ))}
        </select>
        {error && (
          <span role="alert" className={styles.error}>
            {error.message}
          </span>
        )}
      </div>
    )}
  />
);

export default SelectInput;
