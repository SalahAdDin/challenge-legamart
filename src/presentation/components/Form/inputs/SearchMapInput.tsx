import 'leaflet-geosearch/dist/geosearch.css';
import 'leaflet/dist/leaflet.css';

import { GeometryT } from '@domain/location.dto';
import { Control, LeafletEvent } from 'leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import { SearchResult } from 'leaflet-geosearch/dist/providers/provider';
import { useEffect, useState } from 'react';
import { Controller, FieldValues, UseControllerProps } from 'react-hook-form';
import { MapContainer, TileLayer, useMap } from 'react-leaflet';

import styles from '../form.module.css';

const LeafLetGeoSearch = ({
  onLocationResult,
}: {
  onLocationResult: (latLng: GeometryT) => void;
}) => {
  const provider = new OpenStreetMapProvider();

  const controlOptions = {
    provider,
    style: 'bar',
    autoClose: true,
    keepResult: true,
    zoomLevel: 7,
    classNames: {
      form: styles.map_input_form,
      input: styles.map_input_input,
    },
  };

  const searchControl: Control = GeoSearchControl(controlOptions);

  const map = useMap();

  // TODO: Handle it to be settled up once
  map.on('geosearch/showlocation', (result) => {
    if (result) {
      const {
        location: { x, y },
      } = result as LeafletEvent & { location: SearchResult };

      onLocationResult({ lat: y, lng: x });
    }
  });

  useEffect(() => {
    map.addControl(searchControl);

    return () => {
      map.removeControl(searchControl);
    };
  }, []);

  return null;
};

const SearchMapInput = <T extends FieldValues>({
  name,
  control,
  label,
}: UseControllerProps<T> & { label: string }) => {
  const [center, setCenter] = useState<GeometryT>();

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      });
    }
  }, []);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange }, fieldState: { error } }) => (
        <div className={styles.field}>
          <label htmlFor={name}>{`${label}:`}</label>
          {center !== undefined && (
            <MapContainer
              scrollWheelZoom
              zoom={10}
              className={styles.map_input_map}
              center={center}
              id={name}
            >
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <LeafLetGeoSearch onLocationResult={onChange} />
            </MapContainer>
          )}
          {error && (
            <span role="alert" className={styles.error}>
              {error.message}
            </span>
          )}
        </div>
      )}
    />
  );
};

export default SearchMapInput;
