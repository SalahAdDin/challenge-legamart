import Icon from '@presentation/components/icons/Icon';
import UploadIcon from '@presentation/components/icons/UploadIcon';
import clsx from 'clsx';
import { Controller, FieldValues, UseControllerProps } from 'react-hook-form';

import styles from '../form.module.css';

/*
  TODO: Handle validation in way it is possible to pass value to the controlled input.
  ref: https://github.com/react-hook-form/react-hook-form/discussions/1946#discussioncomment-2976570
*/
const FileInput = <T extends FieldValues>({
  name,
  control,
  label,
  accept,
}: UseControllerProps<T> & { label: string; accept: string }) => (
  <Controller
    name={name}
    control={control}
    render={({ field, fieldState: { error } }) => (
      <div className={clsx(styles.field, styles.file_field)}>
        <span>{`${label}:`}</span>
        <label htmlFor={name}>
          Upload
          <Icon icon={UploadIcon} style={{ margin: 'auto' }} />
          <input
            type="file"
            name={field.name}
            id={field.name}
            accept={accept}
            onChange={(e) => {
              const { files } = e.target;

              if (files !== null && files[0] !== null) {
                field.onChange(files[0]);
              }
            }}
          />
        </label>
        {error && (
          <span role="alert" className={styles.error}>
            {error.message}
          </span>
        )}
      </div>
    )}
  />
);

export default FileInput;
