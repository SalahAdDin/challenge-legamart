import { Controller, FieldValues, UseControllerProps } from 'react-hook-form';

import styles from '../form.module.css';

const TextInput = <T extends FieldValues>({
  name,
  control,
  label,
}: UseControllerProps<T> & { label: string }) => (
  <Controller
    name={name}
    control={control}
    render={({ field, fieldState: { error } }) => (
      <div className={styles.field}>
        <label htmlFor={name}>{`${label}:`}</label>
        <input {...field} type="text" />
        {error && (
          <span role="alert" className={styles.error}>
            {error.message}
          </span>
        )}
      </div>
    )}
  />
);

export default TextInput;
