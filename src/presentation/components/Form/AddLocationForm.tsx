import AppContext from '@application/context';
import { blobToBase64, onPromise } from '@application/utils';
import { GeometryT, locationTypes, LocationTypeT } from '@domain/location.dto';
import { yupResolver } from '@hookform/resolvers/yup';
import {
  FOLDER_TARGET,
  UPLOAD_PRESET,
  uploadImage,
} from '@infrastructure/image.api';
import { nanoid } from 'nanoid';
import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';

import Button from '../button/Button';
import styles from './form.module.css';
import FileInput from './inputs/FileInput';
import SearchMapInput from './inputs/SearchMapInput';
import SelectInput from './inputs/SelectInput';
import TextInput from './inputs/TextInput';
import schema from './validation.schema';

type AddLocationFormProps = {
  name: string;
  type: LocationTypeT;
  logo?: File;
  location?: GeometryT;
};

const defaultValues: AddLocationFormProps = {
  name: '',
  type: 'home',
  location: undefined,
  logo: undefined,
};

const locationTypesOptions = locationTypes.map((type) => ({
  value: type,
  label: type,
}));

const AddLocationForm: React.FC = () => {
  const { addLocation } = useContext(AppContext);

  const form = useForm<AddLocationFormProps>({
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { control, handleSubmit, reset } = form;

  // Callback version of watch.  It's your responsibility to unsubscribe when done.
  /*
  * For development purposes.
  useEffect(() => {
    const subscription = watch((value, { name, type }) =>
      console.log(value, name, type),
    );
    return () => subscription.unsubscribe();
  }, [watch]);
  */

  const onSubmit = handleSubmit(async (data) => {
    const { name, type, location, logo } = data;

    if (location !== undefined && logo && logo !== undefined) {
      try {
        const base64Logo = await blobToBase64(logo);
        if (base64Logo !== undefined) {
          const uploadBody = {
            file: `data:${logo.type};base64${base64Logo}`,
            upload_preset: UPLOAD_PRESET,
            folder: FOLDER_TARGET,
          };
          const response = await uploadImage(uploadBody);

          addLocation({
            id: nanoid(),
            name,
            type,
            location,
            logo: response.url,
          });
          reset();
        }
      } catch (error) {
        console.log('Error uploading file: ', error);
      }
    }
    // onclose();
  });

  const onReset = () => {
    reset();
    // onClose();
  };

  return (
    <form
      onSubmit={onPromise(onSubmit)}
      onReset={onReset}
      className={styles.form}
    >
      <header>
        <h1>Share Location</h1>
      </header>
      <main>
        <TextInput name="name" control={control} label="Location Name" />
        <SearchMapInput
          name="location"
          control={control}
          label="Location on Map"
        />
        <SelectInput
          name="type"
          control={control}
          label="Location Type"
          options={locationTypesOptions}
        />
        <FileInput
          name="logo"
          control={control}
          label="Location Logo"
          accept="image/*"
        />
      </main>
      <footer>
        <Button type="submit">Submit</Button>
        <Button secondary onClick={onReset}>
          Cancel
        </Button>
      </footer>
    </form>
  );
};

export default AddLocationForm;
