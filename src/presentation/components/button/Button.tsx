import clsx from 'clsx';
import React from 'react';

import styles from './button.module.css';

type ButtonProps = {
  onClick?: (se: React.SyntheticEvent) => void;
  children: React.ReactNode;
  secondary?: boolean;
  disabled?: boolean;
  type?: 'submit' | 'reset' | 'button';
};

const Button: React.FC<ButtonProps> = ({
  children,
  secondary = false,
  disabled = false,
  type = 'button',
  onClick,
}) => (
  <button
    className={clsx(styles.button, {
      [styles.secondaryButton]: secondary,
      [styles.disabledButton]: disabled,
    })}
    // eslint-disable-next-line react/button-has-type
    type={type}
    onClick={onClick}
    disabled={disabled}
  >
    {children}
  </button>
);

export default Button;
