import AppProvider from '@application/provider';
import AddLocationForm from '@presentation/components/form/AddLocationForm';
import LoadingWidget from '@presentation/components/LoadingWidget';
import MapSection from '@presentation/components/map/MapSection';
import { Suspense } from 'react';

import styles from './App.module.css';

const App = () => (
  <Suspense fallback={<LoadingWidget />}>
    <AppProvider>
      <main className={styles.app}>
        <aside>
          <AddLocationForm />
        </aside>
        <section>
          <MapSection />
        </section>
      </main>
    </AppProvider>
  </Suspense>
);

export default App;
