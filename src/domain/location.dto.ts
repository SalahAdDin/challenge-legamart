const locationTypes = <const>['home', 'hotel', 'restaurant', 'office'];

type LocationTypeT = typeof locationTypes[number];

type GeometryT = {
  lat: number;
  lng: number;
};

type LocationT = {
  id: string;
  name: string;
  location: GeometryT;
  type: LocationTypeT;
  logo: string;
};

export type { GeometryT, LocationT, LocationTypeT };
export { locationTypes };
