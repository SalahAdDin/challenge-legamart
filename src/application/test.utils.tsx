import { LocationT } from '@domain/location.dto';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import React from 'react';

import AppContext, { AppContextProps } from './context';

interface CustomRenderOptions {
  providerProps: AppContextProps;
  renderOptions?: Omit<RenderOptions, 'wrapper'>;
}

const dummyLocation: LocationT = {
  id: 'dummy',
  name: 'Dummy Name',
  location: { lat: 47.5490251, lng: 1.7324062 },
  type: 'office',
  logo: 'https://belapay.com/wp-content/uploads/2021/04/166-1665242_fakery-logo-transparent-dummy-logo-png-clipart.jpg',
};

function customRender(
  ui: React.ReactElement,
  { providerProps, renderOptions }: CustomRenderOptions,
): RenderResult {
  return render(ui, {
    wrapper: ({ children }: { children: React.ReactElement }) => (
      <AppContext.Provider value={providerProps}>
        {children}
      </AppContext.Provider>
    ),
    ...renderOptions,
  });
}

export { customRender, dummyLocation };
