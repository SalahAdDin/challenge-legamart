import { SyntheticEvent } from 'react';

function onPromise<T>(promise: (event: SyntheticEvent) => Promise<T>) {
  return (event: SyntheticEvent) => {
    if (promise) {
      promise(event).catch((error) => {
        console.log('Unexpected error', error);
      });
    }
  };
}

function blobToDataUrl(blob: File): Promise<string | undefined> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result?.toString());
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
}
const blobToBase64 = (blob: File): Promise<string | undefined> =>
  blobToDataUrl(blob).then((text) => text?.slice(text.indexOf(',')));

export { blobToBase64, onPromise };
