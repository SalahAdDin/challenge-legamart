import '@testing-library/jest-dom';

import { LocationT } from '@domain/location.dto';
import { render, screen } from '@testing-library/react';
import React from 'react';

import AppContext, { AppContextProps, initialState } from './context';
import { customRender, dummyLocation } from './test.utils';

const AppConsumer: React.FC = () => (
  <AppContext.Consumer>
    {({ locations }) => (
      <ul>
        {locations.length > 0 ? (
          locations.map((location) => (
            <li key={location.id}>
              <p>{location.id}</p>
              <p>{location.name}</p>
              <p>{`${location.location.lat}, ${location.location.lng}`}</p>
              <p>{location.type}</p>
              <p>{location.logo}</p>
            </li>
          ))
        ) : (
          <li>There is no locations to show</li>
        )}
      </ul>
    )}
  </AppContext.Consumer>
);

describe('App Context', () => {
  const setup = (locations: LocationT[]) => {
    const providerProps: AppContextProps = {
      locations,
      addLocation: () => undefined,
    };
    customRender(<AppConsumer />, { providerProps });
  };

  test('should show default values', () => {
    render(<AppConsumer />);

    expect(
      screen.getByText('There is no locations to show'),
    ).toBeInTheDocument();
  });

  test('should show default values from the context', () => {
    setup(initialState);

    expect(
      screen.getByText('There is no locations to show'),
    ).toBeInTheDocument();
  });

  test('should show a location from the context', () => {
    const { id, name, type, location, logo } = dummyLocation;
    setup([dummyLocation]);

    expect(screen.getByText(id)).toBeInTheDocument();
    expect(screen.getByText(name)).toBeInTheDocument();
    expect(
      screen.getByText(`${location.lat}, ${location.lng}`),
    ).toBeInTheDocument();
    expect(screen.getByText(type)).toBeInTheDocument();
    expect(screen.getByText(logo)).toBeInTheDocument();
  });
});
