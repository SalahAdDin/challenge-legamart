import { LocationT } from '@domain/location.dto';
import { createContext } from 'react';

const initialState: LocationT[] = [];

type AppContextProps = {
  locations: LocationT[];
  addLocation: (locations: LocationT) => void;
};

const AppContext = createContext<AppContextProps>({
  locations: initialState,
  addLocation: () => undefined,
});

export type { AppContextProps };
export { initialState };
export default AppContext;
