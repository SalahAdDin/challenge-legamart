import { LocationT } from '@domain/location.dto';
import React, { useMemo, useState } from 'react';
import AppContext, { initialState } from './context';

const AppProvider: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  const [locations, setLocations] = useState(initialState);

  const addLocation = (location: LocationT) => {
    setLocations((prevState) => [...prevState, location]);
  };

  const memo = useMemo(() => ({ locations, addLocation }), [locations]);

  return <AppContext.Provider value={memo}>{children}</AppContext.Provider>;
};

export default AppProvider;
