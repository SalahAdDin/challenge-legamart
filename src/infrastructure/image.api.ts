import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

const CLOUD_NAME = 'dqhx2k8cf';
const UPLOAD_PRESET = 'sqtmgmb0';
const FOLDER_TARGET = 'legamart';

const BASE_URL = `https://api.cloudinary.com/v1_1/${CLOUD_NAME}`;

const axiosConfig: AxiosRequestConfig = {
  baseURL: BASE_URL,
};

const client: AxiosInstance = axios.create(axiosConfig);

type UploadOptions = {
  file: string;
  upload_preset: string;
  folder?: string;
};

const uploadImage = async (data: UploadOptions): Promise<{ url: string }> => {
  const result = await client.post<{ url: string }>('/upload', data);

  return result.data;
};

export { FOLDER_TARGET, UPLOAD_PRESET, uploadImage };
